<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
  
<h1>Client List</h1>  
<table border="2" width="70%" cellpadding="2">  
<tr><th>S.N</th><th>Client name</th><th>Client Address</th><th>Deposit</th><th>Loan</th></tr>  
   <c:forEach var="client" items="${list}">   
   <tr>  
   <td>${client.id}</td>
   <td>${client.clientname}</td>  
   <td>${client.clientaddress}</td>  
   <td>${client.deposit}</td>  
   <td>${client.loan}</td>  
   <td><a href="editclient${client.id}.ds">Edit</a></td>  
   <td><a href="deleteclient${client.id}.ds">Delete</a></td>
   
   </tr>  
   </c:forEach>  
   </table>  
   <br/>  
   <a href="addclient.ds">Add New Client</a>   --%>
   
   <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
  <link href="css/jquery.dataTables.min.css" rel="stylesheet">
 
 
</head>
<body>
<div class="container">
<%@include file="/common/header1.jsp" %>
<%@include file="/common/nav.jsp" %>
<div class="row">
<%@include file="/common/sidebar.jsp" %>


<div class="col-md-9"  style="height: 500px; color: black;">
<div class="container-fluid">
  <div class="panel panel-default">
  <div class="panel-heading"><div class="panel-title">Client List<a style="margin-bottom:10px;" class="pull-right" href="addclient.ds"><span class="glyphicon glyphicon-plus" ></span></a></div></div>
    <div class="panel-body">
    <table id="myTable" class="table table-striped table-bordered">
        <%int sn=0; %>
        <thead>
          <tr>
                <th>id</th>
                <th>Client Name</th>
                <th>Client Address</th>
                <th>Deposit</th>
                <th>Loan</th>
                <th>Action</th>
                
         </tr>
            </thead>
             <c:forEach var="client" items="${clients}">
             <% sn+=1;%>   
   <tr>  
   <td><% out.print(sn);%></td>
   <td>${client.clientname}</td>  
   <td>${client.clientaddress}</td>  
   <td>${client.deposit}</td>  
   <td>${client.loan}</td>  
   <td>
   	   <a href="detailview${client.id}.ds" class="btn btn-success"><i class="glyphicon glyphicon-eye-open"></i></a>
	   <a href="editclient${client.id}.ds" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
	   <a href="deleteclient${client.id}.ds" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
	  
	   
   </td>
   
   </tr>  
   
   </c:forEach>  
            
   </table>  

</div>
</div>
</div>
</div>
</div>
</div>
 <script src="js/jquery-3.2.1.min.js"></script> 
  <script src="js/bootstrap.min.js"></script>
  <script src="js/myfile.js"></script>
  <script src="js/jquery.dataTables.min.js"></script> 
  <script type="text/javascript">
  
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
</body>
</html>
<!-- <form action="/SpringProject1/register.ds" method="POST">
<h1>Add Client</h1>  
Name:
<br/><input type="text" name="clientname" value=""/><br/>  
Address:
<br/><input type="text" name="clientaddress"/><br/>  
deposit:
<br/><input type="text" name="deposit" value="0"/><br/>  
loan:
<br/><input type="text" name="loan" value="0"/><br/>  

<br/>
<input type="submit" value="register"/>  
</form>   -->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
 
</head>
<body>
<div class="container">
<%@include file="/common/header1.jsp" %>
<%@include file="/common/nav.jsp" %>
<div class="row">
<%@include file="/common/sidebar.jsp" %>




<div class="col-md-9">
  
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Add New Customer</div>
            </div>  
            <div class="panel-body" >
                <form method="post" action="/SpringProject1/register.ds">
               
                      <div id="div_id_username" class="form-group required">
                            <label class="control-label col-md-4  requiredField">Client Name: </label>
                            <div class="controls col-md-8 ">
                                <input class="input-md  textinput textInput form-control" maxlength="30" name="clientname" placeholder="Place name here!!" style="margin-bottom: 10px" type="text" />
                            </div>
                        </div>
                        
                       <div class="form-group required">
                            <label  class="control-label col-md-4  requiredField">Client Address: </label>
                            <div class="controls col-md-8 ">
                                <input class="input-md  textinput textInput form-control"  maxlength="30" name="clientaddress" placeholder="Place address here!!" style="margin-bottom: 10px" type="text" />
                            </div>
                        </div>
                        
                        <div class="form-group required">
                            <label class="control-label col-md-4  requiredField"> Deposit: </label>
                            <div class="controls col-md-8 ">
                                <input class="input-md  textinput textInput form-control" name="deposit" placeholder="Client deposited amount!!" style="margin-bottom: 10px" type="text" />
                            </div>     
                        </div>
                       
                   
                        <div class="form-group required">
                            <label  class="control-label col-md-4  requiredField"> Loan: </label>
                            <div class="controls col-md-8 ">
                                <input class="input-md  textinput textInput form-control"  name="loan" placeholder="Client loan amount!!" style="margin-bottom: 10px" type="text" />
                            </div>     
                        </div>
                   
                        <div class="form-group"> 
                            <div class="aab controls col-md-4 "></div>
                            <div class="controls col-md-8 " style="margin-top: 10px;">
                                <input type="submit" name="register" value="Register" class="btn btn-primary btn btn-primary" />
                             
                            </div>
                        </div> 
                            
                    </form>

            </div>
        </div>
</div>
</div>
    </div>
  <script src="/js/jquery-3.2.1.min.js"></script> 
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/myfile.js"></script>
</body>
</html>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<h3>Loans</h3>
<table border="2" width="70%" cellpadding="2">  
<tr><th>S.N</th><th>Client name</th><th>Amount</th><th>Date</th></tr>  
   <c:forEach var="client" items="${loans}">   
   <tr>  
   <td>${client.id}</td>
   <td>${client.client}</td>  
   <td>${client.amount}</td>  
   <td>${client.date}</td>  
  
   </tr>  
   </c:forEach>  
   </table>  
 --%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
  <link href="css/jquery.dataTables.min.css" rel="stylesheet">
 
 
</head>
<body>
<div class="container">
<%@include file="/common/header1.jsp" %>
<%@include file="/common/nav.jsp" %>
<div class="row">
<%@include file="/common/sidebar.jsp" %>


<div class="col-md-9"  style="height: 500px; color: black;">
<div class="container-fluid">
  <div class="panel panel-default">
  <div class="panel-heading"><div class="panel-title">Loan List<a style="margin-bottom:10px;" class="pull-right" href="addclient.ds"><span class="glyphicon glyphicon-plus" ></span></a></div></div>
    <div class="panel-body">
    <table id="myTable" class="table table-striped table-bordered">
        <%int sn=0; %>
        <thead>
          <tr>
                <th>S.N</th>
                <th>Client Name</th>
                <th>Amount</th>
                <th>Date</th>
                
         </tr>
            </thead>
   <c:forEach var="client" items="${loans}">   
   <%sn+=1; %>
   <tr>  
   <td><%out.print(sn); %></td>
   <td>${client.client}</td>  
   <td>${client.amount}</td>  
   <td>${client.date}</td>  
  
   </tr>  
   </c:forEach>  
   </table>  

</div>
</div>
</div>
</div>
</div>
</div>
 <script src="js/jquery-3.2.1.min.js"></script> 
  <script src="js/bootstrap.min.js"></script>
  <script src="js/myfile.js"></script>
  <script src="js/jquery.dataTables.min.js"></script> 
  <script type="text/javascript">
  
$(document).ready(function(){
    $('#myTable').DataTable();
});
</script>
</body>
</html>
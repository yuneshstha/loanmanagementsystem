<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
  
        <h1>Edit Client Details</h1>  
       <form:form method="POST" action="/SpringProject1/editsave.ds">    
        <table >    
        <tr>  
        <td></td>    
         <td><form:hidden  path="id" /></td>  
         </tr>   
         <tr>    
          <td>Clientname : </td>   
          <td><form:input path="clientname"  /></td>  
         </tr>    
         <tr>    
          <td>Clientaddress :</td>    
          <td><form:input path="clientaddress" /></td>  
         </tr>
         <tr>    
          <td>Deposit : </td>   
          <td><form:input path="deposit"  /></td>  
         </tr>
         <tr>    
          <td>Loan: </td>   
          <td><form:input path="loan"  /></td>  
         </tr>   
              
         <tr>    
          <td> </td>    
          <td><input type="submit" value="Edit Save" /></td>    
         </tr>    
        </table>    
       </form:form>     --%>
       
       
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
   <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
 
</head>
<body>
<div class="container">
<%@include file="/common/header1.jsp" %>
<%@include file="/common/nav.jsp" %>
<div class="row">
<%@include file="/common/sidebar.jsp" %>


<div class="col-md-9"  style="height: 500px; color: black;">
<div class="container-fluid">
  <div class="panel panel-default">
  <div class="panel-heading"><div class="panel-title">Edit Client</div></div>
    <div class="panel-body">
    
    <form:form method="POST" action="/SpringProject1/editsave.ds">    
        <table >    
        <tr>  
        <td></td>    
         <td><form:hidden  path="id" /></td>  
         </tr>   
         <div class="form-group required">
                            <label class="control-label col-md-4  requiredField">Client Name:</label>
                            <div class="controls col-md-8 ">
                               <form:input class="input-md textinput textInput form-control" path="clientname" style="margin-bottom: 10px" type="text"/>
                            </div>
                        </div> 
         <div class="form-group required">
                            <label class="control-label col-md-4  requiredField">Client Address:</label>
                            <div class="controls col-md-8 ">
                               <form:input class="input-md textinput textInput form-control" path="clientaddress" style="margin-bottom: 10px" type="text"/>
                            </div>
                        </div> 
                        
         <div class="form-group required">
                            <label class="control-label col-md-4  requiredField">Deposit:</label>
                            <div class="controls col-md-8 ">
                               <form:input class="input-md textinput textInput form-control" path="deposit" style="margin-bottom: 10px" type="text"/>
                            </div>
                        </div> 
                        
          <div class="form-group required">
                            <label class="control-label col-md-4  requiredField">Loan:</label>
                            <div class="controls col-md-8 ">
                               <form:input class="input-md textinput textInput form-control" path="loan" style="margin-bottom: 10px" type="text"/>
                            </div>
                        </div> 
                       
                       <div class="controls col-md-8 " style="margin-top: 10px;">
                                <input type="submit"  value="Edit Save" class="btn btn-primary btn btn-primary" />
                             
                            </div>
                           
        </table>    
       </form:form>  
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>   
   <script src="/js/jquery-3.2.1.min.js"></script> 
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/myfile.js"></script>
  </body>
  </html>  
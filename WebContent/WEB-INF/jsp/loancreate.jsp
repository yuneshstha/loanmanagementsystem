<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
  <link href="css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="css/datetimepicker.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <%@include file="/common/header1.jsp" %>
    <%@include file="/common/nav.jsp" %>
    <div class="row">
      <%@include file="/common/sidebar.jsp" %>
    

        <div class="col-md-9"  style="color: black;">

          <form class="form-group" action="/SpringProject1/loanpassed.ds" method="post">
            
            <div class="form-group required" style="margin-top: 10px;">
                <label class="control-label col-md-4  requiredField">Loan Amount: </label>
                <div class="controls col-md-8 ">
                    <input class="input-md  textinput textInput form-control" name="amount" style="margin-bottom: 10px" type="text" />
                </div>
            </div>
            <div class="form-group required" style="margin-bottom: 10px;">
              <div class="form-group">
                  <label  class="control-label col-md-4  requiredField">Select Client:</label>
                  <div class="controls col-md-8" style="margin-top: 10px;">
                    <select name="client" class="form-control" data-live-search="true">
                      <c:forEach var="loan" items="${list}">
                        <option> 
							${loan.clientname}
                        </option>
                      </c:forEach>
                    </select>
                  </div> 
              </div>
            </div>   
              <div class="form-group required"">
              <label  class="control-label col-md-4  requiredField"style="margin-top: 10px;">Loan Date: </label>
              <div class="controls col-md-8 "style="margin-top: 10px;">
                <div class='input-group date' id='datetimepicker1'>
                  <input type='text' name="date" class="form-control" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>                      
              </div>
            </div> 
            
      
            <div class="form-group"> 
              <div class="controls col-md-8 " style="margin-top: 10px;">
                  <input type="submit" value="Pass Loan" class="btn btn-primary btn btn-primary" />
              </div>
            </div> 
                                
          </form> 
        </div>
      </div>
    </div>
    
  <script src="js/jquery-3.2.1.min.js"></script> 
  <script src="js/bootstrap.min.js"></script>
  <script src="js/myfile.js"></script>
  <script src="js/jquery.dataTables.min.js"></script> 
  <script src="js/datetimepicker.js"></script> 

  <script type="text/javascript">
  
  $(document).ready(function(){
      $('#myTable').DataTable();
  });
  
   $(function () {
      $('#datetimepicker1').datetimepicker();
  });
 
</script>
</body>
</html>
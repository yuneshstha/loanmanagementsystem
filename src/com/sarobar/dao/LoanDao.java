package com.sarobar.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.sarobar.beans.Client;
import com.sarobar.beans.Loan;

public class LoanDao {
private JdbcTemplate template;
	
	public void setTemplate(JdbcTemplate template)
	{
		this.template=template;
	}
	
	public int save(Loan l)
	{
		System.out.println("loan save called");
		System.out.println(l.getClient());
		System.out.println(l.getDate());
		
		String sql="insert into loan(client,amount,date) values ('"+l.getClient()+"','"+l.getAmount()+"','"+l.getDate()+"')";
		return template.update(sql);
	}
	
	public List<Loan> getLoan()
	{
		System.out.println("hello");
	    return template.query("select * from loan",new RowMapper<Loan>(){  
	        public Loan mapRow(ResultSet rs, int row) throws SQLException {  
	            Loan l=new Loan();  
	            l.setId(rs.getInt(1));
	            l.setClient(rs.getString(2));
	            l.setAmount(rs.getFloat(3));
	            l.setDate(rs.getString(4));
	                       return l;  
	        }
	    });
	}
	
	/*public Loan getLoanById(int id)
	{  
		System.out.println(id);
		String sql="select * from loan where id=?";
		
		//System.out.println(sql);
		return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Loan>(Loan.class)); 
		
	}*/
	
	/*public Loan getLoanByName(String client)
	{
		
		System.out.println(client);
		String sql="select * from loan where client="+client+"";
		//System.out.println(sql);
		return template.queryForObject(sql, new Object[]{client},new BeanPropertyRowMapper<Loan>(Loan.class));
		
	}
	*/
	public List<Loan> getLoanByName(String client)
	{
		System.out.println("hello");
	    return template.query("select * from loan where client='"+client+"'",new RowMapper<Loan>(){  
	        public Loan mapRow(ResultSet rs, int row) throws SQLException {  
	            Loan l=new Loan();  
	            l.setId(rs.getInt(1));
	            l.setClient(rs.getString(2));
	            l.setAmount(rs.getFloat(3));
	            l.setDate(rs.getString(4));
	                       return l;  
	        }
	    });
	}
	}

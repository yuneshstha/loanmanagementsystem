package com.sarobar.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;


import com.sarobar.beans.Client;



public class ClientDao {

	private JdbcTemplate template;
	
	public void setTemplate(JdbcTemplate template)
	{
		this.template=template;
	}
	
	
	public List<Client> getClient(){ 
		System.out.println("hello");
	    return template.query("select * from bank",new RowMapper<Client>(){  
	        public Client mapRow(ResultSet rs, int row) throws SQLException {  
	            Client c=new Client();  
	            c.setId(rs.getInt(1));
	            c.setClientname(rs.getString(2));  
	            c.setClientaddress(rs.getString(3));
	            c.setDeposit(rs.getFloat(4));
	            c.setLoan(rs.getFloat(5));
	            //System.out.println(b.getId());
	            //System.out.println(b.getBookname());
	            //System.out.println(b.getBookprice());
	            return c;  
	        }
	    });
	}

	
	public int save(Client c)
	{
		System.out.println("hello save");
		String sql="insert into bank(clientname,clientaddress,deposit,loan) values ('"+c.getClientname()+"','"+c.getClientaddress()+"','"+c.getDeposit()+"','"+c.getLoan()+"')";
		
		return template.update(sql);
		
	}
	
	public int delete(int id)
	{
		String sql="delete from bank where id="+id+"";
		return template.update(sql);
	}
	
	public Client getClientById(int id)
	{  
		System.out.println(id);
	    String sql="select * from bank where id=?";  
	    
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Client>(Client.class)); 
	
	}
	
	public int update(Client client)
	{
		String sql="update bank set clientname='"+client.getClientname()+"', clientaddress='"+client.getClientaddress()+"',deposit='"+client.getDeposit()+"',loan='"+client.getLoan()+"' where id="+client.getId()+"";
		return template.update(sql);
				
	}
	
	/*public Client getClientByName(String clientname)
	{  
		System.out.println(clientname);
	    String sql="select * from bank where clientname=?";  
	    
	    return template.queryForObject(sql, new Object[]{clientname},new BeanPropertyRowMapper<Client>(Client.class)); 
	
	}*/
	
	
}

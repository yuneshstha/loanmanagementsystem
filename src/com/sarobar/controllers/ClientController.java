package com.sarobar.controllers;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.sarobar.beans.Client;
import com.sarobar.dao.ClientDao;

@Controller
public class ClientController {
	
	@Autowired
	ClientDao dao;
	
	
	
	
	@RequestMapping(value="/addclient")
	public ModelAndView addclient()
	{
		
		System.out.println("add clients here");
		return new ModelAndView("addclient","command",new Client());
		
	}
	
	@RequestMapping(value="/viewclient")
	public ModelAndView viewclient()
	{
		System.out.println("viewing client");
		List<Client> list=dao.getClient();
		
		return new ModelAndView("viewclient","clients",list);
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public ModelAndView register(@ModelAttribute("client") Client client)
	{
		System.out.println("register called");
		dao.save(client);
		return new ModelAndView("redirect:/viewclient.ds");
		
	}
	
	@RequestMapping(value="/deleteclient{id}",method=RequestMethod.GET)
	public ModelAndView deleteclient(@PathVariable int id)
	{
		
		System.out.println("delete data");
		System.out.println(id);
		
		dao.delete(id);
		return new ModelAndView("redirect:/viewclient.ds");
	}
	/*public String delete()
	{
		System.out.println("delete data");
		return "addclient";
	}*/
	
	@RequestMapping(value="/editclient{id}",method=RequestMethod.GET)
	public ModelAndView editclient(@PathVariable int id)
	{
		
		System.out.println("editclient called");
		
		Client client=dao.getClientById(id);
		return new ModelAndView("clientedit","command",client);
		
	}
	
	@RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public ModelAndView editsave(@ModelAttribute("Client") Client client){  
    	System.out.println("editsave");
    	dao.update(client);  
        return new ModelAndView("redirect:/viewclient.ds");  
        
    }  
	
	@RequestMapping(value="/detailview{id}",method=RequestMethod.GET)
	public ModelAndView viewClientDetails(@PathVariable int id)
	{
		System.out.println("detailed view called");
		Client client=dao.getClientById(id);
		

		//System.out.println(client.getLoanByClient());
		//String name=client.getClientname();
		
		
		return new ModelAndView("detailview","client",client);
	}
	
	/*@RequestMapping(value="/detailview{clientname}",method=RequestMethod.GET)
	public ModelAndView viewClientDetails(@PathVariable String clientname)
	{
		System.out.println("detailed view called");
		Client client=dao.getClientByName(clientname);
		

		//System.out.println(client.getLoanByClient());
		//String name=client.getClientname();
		
		
		return new ModelAndView("detailview","client",client);
	}
	*/
	
}

package com.sarobar.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sarobar.beans.Client;
import com.sarobar.beans.Loan;
import com.sarobar.dao.ClientDao;
import com.sarobar.dao.LoanDao;

@Controller
public class LoanController {

	@Autowired 
	ClientDao dao;
	@Autowired
	LoanDao dao1;
	
	@RequestMapping(value="/loancreate")
	public ModelAndView loandate()
	{
		 List<Client> list=dao.getClient();
		System.out.println("section entered");
		
		return new ModelAndView("loancreate","list",list);
	}
	
	@RequestMapping(value="/loanpassed" , method=RequestMethod.POST)
	public ModelAndView loanview(@ModelAttribute("loan") Loan l)
	{
		System.out.println("loan passed");
		dao1.save(l);
		return new ModelAndView("loanpassed");
	}
	
	@RequestMapping(value="/viewloan")
	public ModelAndView viewclient()
	{
		System.out.println("viewing loan");
		List<Loan> list=dao1.getLoan();
		return new ModelAndView("viewloan","loans",list);
	}
	
/*	@RequestMapping(value="/indivisualloan{id}",method=RequestMethod.GET)
	public ModelAndView editLoan(@PathVariable int id)
	{
		
		System.out.println("indivisual loan");
		
		//Client client=dao.getClientById(id);
		Loan loan=dao1.getLoanById(id);
		
		return new ModelAndView("indivisualloan","loan",loan);
		
	}
*/	
	@RequestMapping(value="/indivisualloan{client}",method=RequestMethod.GET)
	public ModelAndView viewallloans(@PathVariable String client)
	{
		
		System.out.println("indivisual loan display");
		
		//Client client=dao.getClientById(id);
		System.out.println(client);
		List<Loan> loan=dao1.getLoanByName(client);
		
		//Loan loan=dao1.getLoanByName(client);
		System.out.println(loan);
		//System.out.println(loan.getAmount());
		
		return new ModelAndView("indivisualloan","loans",loan);
		
	}
	
	
	
		
	
}
